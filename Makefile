#
# GNU Makefile
#

PROGRAM=lifetime

CC=gcc
CFLAGS=-O2 -g0 -Wall -Wextra -Wno-unused-variable

all: $(PROGRAM)

$(PROGRAM): $(PROGRAM).o
	$(CC) $^ -o $@ -lm

clean:
	rm -f $(PROGRAM) *.o ERRS

.PHONY: all clean

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
