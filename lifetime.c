//
//  Lifetime Measurer
//
//  by Andrew Apted, 2018.
//
//  License of this code is CC0 (public domain).
//  USE AT OWN RISK.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>

#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <errno.h>

#define AJ_RANDOM_IMPLEMENTATION
#include "aj_random.h"

aj_rand_t RNG;


#define MAX_ITERATION  40000

#define RUNS_PER_DENSITY  4000

#define DISPLAY_SLEEP_MS  20

typedef unsigned char byte;


#define W  64
#define P  (W/8)


// grid_t represents a block of WxW cells.
typedef struct {
	byte data[P*W];
} grid_t;


// how many grids to keep in memory, used to detect when a block
// has been repeated (and hence when the "game" is "over").
// [ we need one extra though ]
#define GRID_HISTORY  6

grid_t grids[GRID_HISTORY+1];


// the list of step counts (lifetimes) for the current density.
int lifetimes[RUNS_PER_DENSITY];

int total_skipped = 0;


// this lookup table is used to speed up the simulation.
// large enough to compute the results for a whole byte (8 cells)
// at once, i.e. 2^30 (around 10^9) entries since we need 10 bits
// from three rows.
byte *large_lookup;

// this smaller lookup table is used to speed up generating the
// large lookup table.  It does 4 cells at a time, and hence
// indexes are 18 bits (around 250kb).
byte *small_lookup;

// this contains enough to compute a single cell.
byte tiny_lookup[512];


void build_tiny_lookup() {
	int i, b;

	for (i=0; i < 512; i++) {
		int alive = (i & (1 << 4));

		int neighbors = 0;

		// count the neighbors
		for (b = 0; b < 9; b++) {
			if ((b != 4) && (i & (1 << b))) {
				neighbors++;
			}
		}

		alive = (neighbors == 3 || (neighbors == 2 && alive));

		tiny_lookup[i] = alive ? 0xff : 0;
	}
}


void build_small_lookup() {
	int idx, b;

	small_lookup = malloc((size_t)(1 << 18));

	if (!small_lookup) {
		fprintf(stderr, "CANNOT ALLOCATE SMALL LOOKUP: %s\n", strerror(errno));
		exit(1);
	}

	for (idx = 0; idx < (1 << 18); idx++) {
		byte val = 0;

		for (b = 0; b < 4; b++) {
			int row1 = (idx >> (12 + b)) & 7;
			int row2 = (idx >> (6 + b)) & 7;
			int row3 = (idx >> (0 + b)) & 7;

			int t_idx = (row1 << 6) | (row2 << 3) | row3;

			val |= tiny_lookup[t_idx] & (1 << b);
		}

		small_lookup[idx] = val | (val << 4);
	}
}


void build_large_lookup() {
	int idx;

	large_lookup = malloc((size_t)(1 << 30));

	if (!large_lookup) {
		fprintf(stderr, "CANNOT ALLOCATE LOOKUP TABLE: %s\n", strerror(errno));
		exit(1);
	}

	for (idx = 0; idx < (1 << 30); idx++) {
		// right nybble
		int row1 = (idx >> 20) & 63;
		int row2 = (idx >> 10) & 63;
		int row3 = (idx) & 63;

		int s_idx = (row1 << 12) | (row2 << 6) | row3;

		byte val = small_lookup[s_idx] & 0x0F;

		// left nybble
		row1 = (idx >> 24) & 63;
		row2 = (idx >> 14) & 63;
		row3 = (idx >> 4) & 63;

		s_idx = (row1 << 12) | (row2 << 6) | row3;

		val |= small_lookup[s_idx] & 0xF0;

		large_lookup[idx] = val;
	}
}


void free_tables() {
	free(small_lookup);
	free(large_lookup);
}


void grid_clear(grid_t *G) {
	memset(G->data, 0, sizeof(G->data));
}


int grid_sum_block(const grid_t *G, int bx, int by, int bsize) {
	int x, y;
	int count = 0;

	for (y = by; y < by+bsize; y++) {
		for (x = bx; x < bx+bsize; x++) {
			int bit = (1 << (7 ^ (x & 7)));
			int val = G->data[y*P + (x >> 3)];

			if (val & bit) {
				count++;
			}
		}
	}

	return count;
}


void grid_display(const grid_t *G, int step_num) {
	int x, y;
	char line[70];

	int use_W = W;
	int use_H = W;

	if (use_W > 76) use_W = 76;
	if (use_H > 23) use_H = 23;

	memset(line, 0, sizeof(line));

	// ANSI escape to clear screen and position cursor at top left
	fprintf(stderr, "\033[H \033[J step %d\n", step_num);

	for (y=0; y < use_H; y++) {
		for (x=0; x < use_W; x++) {
			int count = grid_sum_block(G, x, y, 1);

			if (count > 0) {
				line[x] = '#';
			} else if (count > 2) {
				line[x] = '%';
			} else if (count > 0) {
				line[x] = '.';
			} else {
				line[x] = ' ';
			}
		}

		fprintf(stderr, "  %s\n", line);
	}

	// sleep for a little while
	struct timespec tm;

	tm.tv_sec = 0;
	tm.tv_nsec = 50*1000*1000;

	nanosleep(&tm, NULL);
}


void grid_populate(grid_t *G, int density) {
	int x, y;

	grid_clear(G);

	for (y=0; y < W; y++) {
		for (x=0; x < W; x++) {
			int bit = (1 << (7 ^ (x & 7)));

			int r = aj_rand_IntRange(&RNG, 0, 999);

			if (r < density) {
				G->data[y*P + (x >> 3)] |= bit;
			}
		}
	}
}


bool grid_is_equal(const grid_t const*A, const grid_t const*B) {
	int cmp = memcmp(&A->data[0], &B->data[0], sizeof(A->data));

	return (cmp == 0);
}


void grid_perform_step(const grid_t const*src, grid_t *dest) {
	int lx, mx, rx; // left, middle, right
	int ty, my, by; // top, middle, bottom

	int L_idx;

	for (my=0; my < W; my++) {
		ty = my - 1;
		by = my + 1;

		if (ty < 0) ty += W;
		if (by >= W) by -= W;

		for (mx=0; mx < P; mx++) {
			lx = mx - 1;
			rx = mx + 1;

			if (lx < 0) lx += P;
			if (rx >= P) rx -= P;

			int row1 =  ((src->data[ty*P + lx] & 1) << 9) |
						((src->data[ty*P + mx]) << 1) |
						((src->data[ty*P + rx] & 128) >> 7);

			int row2 =  ((src->data[my*P + lx] & 1) << 9) |
						((src->data[my*P + mx]) << 1) |
						((src->data[my*P + rx] & 128) >> 7);

			int row3 =  ((src->data[by*P + lx] & 1) << 9) |
						((src->data[by*P + mx]) << 1) |
						((src->data[by*P + rx] & 128) >> 7);

			L_idx = (row1 << 20) | (row2 << 10) | row3;

			dest->data[my*P + mx] = large_lookup[L_idx];
		}
	}
}


int iterate_until_dead(int density) {
	// number of steps
	int step = 0;
	int pos;

	grid_populate(&grids[0], density);

	while (true) {
		int pos;

		for (pos = 0; pos < GRID_HISTORY; pos++) {
			grid_perform_step(&grids[pos], &grids[pos+1]);
			step++;
		}

		if (grid_is_equal(&grids[0], &grids[GRID_HISTORY])) {
			return step;
		}

		if (step >= MAX_ITERATION) {
			return step;
		}

		// copy last one to the front
		memcpy(&grids[0], &grids[GRID_HISTORY], sizeof(grid_t));
	}
}


void iterate_on_screen() {
	grid_populate(&grids[0], 375);

	int step = 0;

	while (true) {
		grid_display(&grids[0], step);
		grid_perform_step(&grids[0], &grids[1]);
		step++;

		grid_display(&grids[1], step);
		grid_perform_step(&grids[1], &grids[0]);
		step++;
	}
}


static int int_compare(const void *A, const void *B) {
	return *(const int *)A - *(const int *)B;
}


void display_medians(FILE *fp, int num) {
	// sort the lifetimes so we can extract median value (etc)
	qsort(&lifetimes[0], num, sizeof(lifetimes[0]), &int_compare);

	// write the data to standard output
	fprintf(fp, "%7d %7d %7d %7d %7d\n",
		lifetimes[num/50],
		lifetimes[num/10],
		lifetimes[num/2],
		lifetimes[num-1 - num/10],
		lifetimes[num-1 - num/50]);

	fflush(fp);
}


void analyse_density(int density) {
	int run;

	size_t num = 0;

	fprintf(stderr, "density %d.%d%%...\n", density/10, density%10);

	for (run = 0; run < RUNS_PER_DENSITY; run++) {
		int steps = iterate_until_dead(density);

		// ignore value when the maximum # of iterations was reached.
		// this happens quite rarely, less than 1% of all runs.
		// it mostly occurs when an oscillator is formed with a period
		// of 5, 7, 8 or other number not divisible into 12, or a glider
		// which never hits anything.

		if (steps >= MAX_ITERATION) {
			total_skipped++;
			continue;
		}

		lifetimes[num++] = steps;
	}

	display_medians(stdout, num);
}

void seed_random_generator() {
	time_t unix_time = time(NULL);

	aj_rand_Seed(&RNG, (uint32_t)unix_time);
}

int main(int argc, char **argv) {
	(void) argc;
	(void) argv;

	fprintf(stderr, "making lookup tables...\n");

	build_tiny_lookup();
	build_small_lookup();
	build_large_lookup();

	seed_random_generator();

	int density;

	for (density = 0; density <= 1000; density += 5) {
		analyse_density(density);
	}

	fprintf(stderr, "total skipped: %d\n", total_skipped);

	free_tables();

	return 0;
}


//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
